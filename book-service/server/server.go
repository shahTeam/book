package server

import (
	"book/bookApp"
	"book/domain/author"
	"book/domain/book"
	"book/domain/category"
	"book/service"
	"context"
	"github.com/google/uuid"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func New(svc service.Service, categoryFactory category.Factory, authorFactory author.Factory, bookFactory book.Factory) Server {
	return Server{
		service:         svc,
		categoryFactory: categoryFactory,
		authorFactory:   authorFactory,
		bookFactory:     bookFactory,
	}
}

type Server struct {
	bookApp.UnimplementedBookServiceServer
	service         service.Service
	categoryFactory category.Factory
	authorFactory   author.Factory
	bookFactory     book.Factory
}

func (s Server) RegisterCategory(ctx context.Context, req *bookApp.CategoryRequest) (*bookApp.Category, error) {
	cat, err := s.categoryFactory.NewCategory(req.Name)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	c, err := s.service.RegisterCategory(ctx, cat)
	if err != nil {
		return nil, err
	}
	return toProtoCategory(c), nil
}

func (s Server) RegisterAuthor(ctx context.Context, req *bookApp.AuthorRequest) (*bookApp.Author, error) {
	log.Println("req: ", req)
	log.Println("reqName", req.LastName)
	aut, err := s.convertRegisterAuthorRequestToDomianAuthor(req)
	if err != nil {
		return nil, err
	}
	crAut, err := s.service.RegisterAuthor(ctx, aut)
	if err != nil {
		return nil, err
	}
	return toProtoAuthor(crAut), nil
}

func (s Server) RegisterBook(ctx context.Context, req *bookApp.BookRequest) (*bookApp.Book, error) {
	book, err := s.convertRegisterRequestToDomianBook(req)
	if err != nil {
		return nil, err
	}

	crBook, err := s.service.RegisterBook(ctx, book)
	if err != nil {
		return nil, err
	}
	return toProtoBook(crBook), nil
}

func (s Server) GetBook(ctx context.Context, req *bookApp.BookIdRequest) (*bookApp.Book, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "id is not uuid")
	}
	b, err := s.service.GetBook(ctx, id)
	if err != nil {
		return nil, err
	}
	return toProtoBook(b), nil
}

func (s Server) UpdateBook(ctx context.Context, req *bookApp.UpdateBookRequest) (*bookApp.Book, error) {
	book, err := s.convertUpdateBookRequestToDomianBook(req)
	if err != nil {
		return nil, err
	}
	b, err := s.service.UpdateBooks(ctx, book)
	if err != nil {
		return nil, err
	}
	return toProtoBook(b), nil
}

func (s Server) DeleteBook(ctx context.Context, req *bookApp.BookIdRequest) (*emptypb.Empty, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return &emptypb.Empty{}, status.Error(codes.InvalidArgument, "id is not uuid")
	}

	if err = s.service.DeleteBook(ctx, id); err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}

func (s Server) convertRegisterAuthorRequestToDomianAuthor(protoAuthor *bookApp.AuthorRequest) (author.Author, error) {
	a, err := s.authorFactory.NewCategory(
		protoAuthor.FirstName,
		protoAuthor.LastName,
		protoAuthor.NickName,
		protoAuthor.Email,
	)
	if err != nil {
		return author.Author{}, status.Error(codes.InvalidArgument, err.Error())
	}
	return a, nil
}

func (s Server) convertUpdateBookRequestToDomianBook(protoBook *bookApp.UpdateBookRequest) (book.Book, error) {
	bookId, err := uuid.Parse(protoBook.Id)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided subject id is not uuid")
	}
	authorId, err := uuid.Parse(protoBook.AuthorId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided subject id is not uuid")
	}

	categoryId, err := uuid.Parse(protoBook.CategoryId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided subject id is not uuid")
	}

	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, err.Error())
	}
	return book.UnmarshalBook(book.UnmarshalBookArgs{
		ID: bookId,
		Name: protoBook.Name,
		AuthorID: authorId,
		CategoryID: categoryId,
		Publisher: protoBook.Publisher,
	})
}

func (s Server) convertRegisterRequestToDomianBook(protoBook *bookApp.BookRequest) (book.Book, error) {
	authorId, err := uuid.Parse(protoBook.AuthorId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided subject id is not uuid")
	}

	categoryId, err := uuid.Parse(protoBook.CategoryId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided subject id is not uuid")
	}

	a, err := s.bookFactory.NewBook(
		protoBook.Name,
		authorId,
		categoryId,
		protoBook.Publisher,
	)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, err.Error())
	}
	return a, nil
}
