package server

import (
	"book/domain/author"
	"book/domain/book"
	"book/domain/category"

	"book/bookApp"
)

func toProtoCategory(c category.Category) *bookApp.Category {
	return &bookApp.Category{
		Id:   c.ID().String(),
		Name: c.Name(),
	}
}

func toProtoAuthor(a author.Author) *bookApp.Author {
	return &bookApp.Author{
		Id:        a.ID().String(),
		FirstName: a.FirstName(),
		LastName:  a.LastName(),
		NickName:  a.NickName(),
		Email:     a.Email(),
	}
}

func toProtoBook(b book.Book) *bookApp.Book {
	return &bookApp.Book{
		Id:         b.ID().String(),
		Name:       b.Name(),
		AuthorId:   b.AuthorID().String(),
		CategoryId: b.CategoryID().String(),
		Publisher:  b.Publisher(),
	}
}
