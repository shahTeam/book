package author

import (
	"book/pkg/id"
	"reflect"
	"testing"

	"github.com/google/uuid"
)

func TestFactory_NewCategory(t *testing.T) {
	type fields struct {
		idGenerator id.IGenerator
	}
	type args struct {
		firstName string
		lastName  string
		nickName  string
		email     string
	}
	tests := []struct {
		name    string
		args    args
		want    Author
		wantErr bool
	}{
		// TODO: Add test cases.
		{
            name: "pass",
			args: args{
                   firstName: "shahzod",
				   lastName: "Ibrohimov",
				   nickName: "Shahzodiy",
				   email: "shahzod@mail.ru",
			},
			want: Author{
                   id: testAuthorID,
				   firstName: "shahzod",
				   lastName: "Ibrohimov",
				   nickName: "Shahzodiy",
				   email: "shahzod@mail.ru",
			},
			wantErr: false,
		},
		{
			name: "first name empty",
			args: args{
				firstName: "",
				lastName: "Ibrohimov",
				nickName: "Shahzpdiy",
				email: "shahzod@mail.ru",
			},
			want: Author{},
			wantErr: true,
		},
       {
		name: "last name empty",
		args: args{
			firstName: "shahzod",
			lastName: "",
			nickName: "Shahzodiy",
			email: "shahzod@mail.ru",
		},
		want: Author{},
		wantErr: true,
	   },
	}

	f := NewFactory(testIDGenerator{})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := f.NewCategory(tt.args.firstName, tt.args.lastName, tt.args.nickName, tt.args.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("Factory.NewCategory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Factory.NewCategory() = %v, want %v", got, tt.want)
			}
		})
	}
}

var testAuthorID = uuid.New()

type testIDGenerator struct{}

func (g testIDGenerator) GenerateUUID() uuid.UUID {
	return testAuthorID
}
