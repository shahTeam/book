package author

import (
	"errors"
	"fmt"

	"book/pkg/validate"
	"github.com/google/uuid"
)

var (
	ErrInvalidAuthorData = errors.New("Invalid Author data!")
)

// Author represents domain object that holds required info for a author
// All core business logic relevant to authors should be done through this struct
type Author struct {
	id        uuid.UUID
	firstName string
	lastName  string
	nickName  string
	email     string
}

func (a Author) ID() uuid.UUID {
	return a.id
}

func (a Author) FirstName() string {
	return a.firstName
}

func (a Author) LastName() string {
	return a.lastName
}
func (a Author) NickName() string {
	return a.nickName
}

func (a Author) Email() string {
	return a.email
}

func (a Author) validate() error {
	if a.firstName == "" {
		return fmt.Errorf("%w: empty first name", ErrInvalidAuthorData)
	}
	if a.lastName == "" {
		return fmt.Errorf("%w: empty last name", ErrInvalidAuthorData)
	}
	if a.nickName == "" {
		return fmt.Errorf("%w: empty nick name", ErrInvalidAuthorData)
	}
	if err := validate.Email(a.email); err != nil {
		return fmt.Errorf("%w:, %v", ErrInvalidAuthorData, err)
	}
	return nil
}

type UnmarshalAuthorArgs struct {
	ID        uuid.UUID
	FirstName string
	LastName  string
	NickName  string
	Email     string
}

func UnmarshalAuthor(args UnmarshalAuthorArgs) (Author, error) {
	a := Author{
		id: args.ID,
		firstName: args.FirstName,
		lastName: args.LastName,
		nickName: args.NickName,
		email: args.Email,
	}
	if err := a.validate(); err != nil {
            return Author{}, err
	}
	return a, nil
}

