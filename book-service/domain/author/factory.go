package author

import "book/pkg/id"

type Factory struct {
	idGenerator id.IGenerator
}

func NewFactory(idGenerator id.IGenerator) Factory {
	return Factory{
		idGenerator: idGenerator,
	}
}

//NewAuthor is a constructor that checks if the provided data for Author is valid or not
//new Author objects can only be created through this constructor which ensure evrything is valid
func (f Factory) NewCategory(
	firstName, lastName, nickName, email string) (Author, error) {
		a := Author{
			id: f.idGenerator.GenerateUUID(),
			firstName: firstName,
			lastName: lastName,
			nickName: nickName,
			email: email,
		}
		if err := a.validate(); err != nil {
			return Author{}, err
		}
		return a, nil
	}