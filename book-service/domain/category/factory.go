package category

import (
	"book/pkg/id"
	"fmt"
	"log"
)

//NewFactory initializes a new Factory
func NewFactory(idGenerator id.IGenerator) Factory {
	return Factory {
		idGenerator: idGenerator,
	}
}

// Factory is a struct that creates new instance of Category. Read more about factory pattern
type Factory struct {
	idGenerator id.IGenerator
}

func (f Factory) NewCategory(name string) (Category, error){
	c := Category{
		id: f.idGenerator.GenerateUUID(),
		name: name,
	}
	fmt.Println(c)
	if err := c.validate(); err != nil {
		log.Println("4:", err)
		return Category{}, err
	}
	return c, nil
}

