package category

import (
	"book/pkg/id"
	"reflect"
	"testing"

	"github.com/google/uuid"
)

func TestFactory_NewCategory(t *testing.T) {
	type fields struct {
		idGenerator id.IGenerator
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    Category
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "pass",
			args: args{
				name: "Horor",
			},
			want: Category{
				id: testCategoryID,
				name: "Horor",
			},
			wantErr: false,
		},
	}
	f := NewFactory(testIDGenerator{})
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := f.NewCategory(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("Factory.NewCategory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Factory.NewCategory() = %v, want %v", got, tt.want)
			}
		})
	}
}

type testIDGenerator struct{}

func (g testIDGenerator) GenerateUUID() uuid.UUID{
	return testCategoryID
}

var testCategoryID = uuid.New()