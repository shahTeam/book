package category

import (
	"errors"
	"fmt"

	"github.com/google/uuid"
)

var (
	// ErrInvalidCategoryData indicates that data passed to create Category instance was invalid
	ErrInvalidCategoryData = errors.New("Invalid category data!")
)

type Category struct {
	id   uuid.UUID
	name string
}

func (c Category) ID() uuid.UUID {
	return c.id
}

func (c Category) Name() string {
	return c.name
}

func (c Category) validate() error {
	if c.name == "" {
		return fmt.Errorf("%w: name is empty", ErrInvalidCategoryData)
	}
	return nil
}

type UnmarshalCategoryArgs struct {
	ID   uuid.UUID
	Name string
}

func UnmarshalCategory(args UnmarshalCategoryArgs) (Category, error) {
	c := Category{
		id: args.ID,
		name: args.Name,
	}
	return c, nil
}
