package book

import (
	"errors"
	"fmt"

	//"github.com/BOOK/book-service/domain/author"
	//"github.com/BOOK/book-service/domain/category"
	"github.com/google/uuid"
)

var (
	ErrInvalidBookData = errors.New("Invalid Book data")
)

type Book struct {
	id         uuid.UUID
	name       string
	authorID   uuid.UUID
	categoryID uuid.UUID
	publisher  string
}

func (b Book) ID() uuid.UUID {
	return b.id
}

func (b Book) Name() string {
	return b.name
}

func (b Book) AuthorID() uuid.UUID {
	return b.authorID
}

func (b Book) CategoryID() uuid.UUID {
	return b.categoryID
}

func (b Book) Publisher() string {
	return b.publisher
}

func (b Book) validate() error {
	if b.name == "" {
		return fmt.Errorf("%w: empty first name", ErrInvalidBookData)
	}
	if b.publisher == "" {
		return fmt.Errorf("%w: empty publisher", ErrInvalidBookData)
	}
	return nil
}

type UnmarshalBookArgs struct {
	ID         uuid.UUID
	Name       string
	AuthorID   uuid.UUID
	CategoryID uuid.UUID
	Publisher  string
}

func UnmarshalBook(args UnmarshalBookArgs) (Book, error) {
	b := Book{
		id:         args.ID,
		name:       args.Name,
		authorID:   args.AuthorID,
		categoryID: args.CategoryID,
		publisher:  args.Publisher,
	}
	if err := b.validate(); err != nil {
		return Book{}, err
	}
	return b, nil
}


