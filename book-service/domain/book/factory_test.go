package book

import (
	//"book/pkg/id"
	"reflect"
	"testing"

	"github.com/google/uuid"
)

func TestFactory_NewBook(t *testing.T) {

	type args struct {
		name       string
		publisher  string
		authorID   uuid.UUID
		categoryID uuid.UUID
	}
	tests := []struct {
		name    string
		args    args
		want    Book
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "pass",
			args: args{
				name: "Goland",
				authorID: testAuthorID,
				categoryID: testCategoryID,
				publisher: "hilolbook",
			},
			want: Book{
                id: testBookID,
				name: "Goland",
				authorID: testAuthorID,
				categoryID: testCategoryID,
				publisher: "hilolbook",
			},
			wantErr: false,
		},
	}

	f := NewFactory(testIDGenerator{})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := f.NewBook(tt.args.name, tt.args.authorID, tt.args.categoryID, tt.args.publisher)
			if (err != nil) != tt.wantErr {
				t.Errorf("Factory.NewBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Factory.NewBook() = %v, want %v", got, tt.want)
			}
		})
	}
}

var	(
	testBookID = uuid.New()
	testAuthorID = uuid.New()
	testCategoryID = uuid.New()
)

type testIDGenerator struct{}

func (g testIDGenerator) GenerateUUID() uuid.UUID {
	return testBookID
}

