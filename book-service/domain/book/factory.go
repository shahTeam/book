package book

import (
	"book/pkg/id"

	"github.com/google/uuid"
)

type Factory struct {
	idGenerator id.IGenerator
}

func NewFactory(idGenerator id.IGenerator) Factory {
	return Factory{
		idGenerator: idGenerator,
	}
}

func (f Factory) NewBook(name string, authorID, categoryID uuid.UUID, publisher string) (Book, error) {
	b := Book {
		id: f.idGenerator.GenerateUUID(),
		name: name,
		authorID: authorID,
		categoryID: categoryID,
		publisher: publisher,
	}
	if err := b.validate(); err != nil {
		return Book{}, err
	}
	return b, nil
}