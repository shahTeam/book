package id

import "github.com/google/uuid"

type IGenerator interface {
	GenerateUUID() uuid.UUID
}

type Generate struct{}

func (g Generate) GenerateUUID() uuid.UUID{
	return uuid.New()
}