package validate

import "net/mail"

func Email(email string) error{
	if _, err := mail.ParseAddress(email); err != nil {
		return err
	}
	return nil
}