package main

import (
	"book/bookApp"
	"book/config"
	"book/domain/author"
	"book/domain/book"
	"book/domain/category"
	"book/pkg/id"
	"book/repository"
	"book/server"
	"book/service"
	"log"
	"net"

	"google.golang.org/grpc"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Print("1: ", err)
	}

	repo, err := repository.NewPostgres(cfg.PostgresConfig)
	if err != nil {
		log.Print("2:", err)
		return
	}

	categoryFactory := category.NewFactory(id.Generate{})
	authorFactory := author.NewFactory(id.Generate{})
	bookFactory := book.NewFactory(id.Generate{})

	svc := service.New(repo, categoryFactory, authorFactory, bookFactory)
	server := server.New(svc, categoryFactory, authorFactory, bookFactory)

	lis, err := net.Listen("tcp", net.JoinHostPort(cfg.Host, cfg.Port))
	if err != nil {
		log.Print("3:", err)
	}

	grpcServer := grpc.NewServer()
	bookApp.RegisterBookServiceServer(grpcServer, server)

	if err := grpcServer.Serve(lis); err != nil {
		log.Print("4:", err)
	}
}
