package repository

import (
	"book/config"
	"book/domain/author"
	"book/domain/book"
	"book/domain/category"
	"context"
	"database/sql"
	"errors"
	"log"

	"book/pkg/errs"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

const (
	categoryTableName = "Category"
	authorTableName   = "Author"
	bookTableName     = "Book"
)

type Postgres struct {
	db *sqlx.DB
}

func NewPostgres(cfg config.PostgresConfig) (*Postgres, error) {
	db, err := connect(cfg)
	if err != nil {
		return nil, err
	}
	return &Postgres{
		db: db,
	}, nil
}

func (p *Postgres) RegisterCategory(ctx context.Context, c category.Category) error {
	return p.creatCategory(ctx, toRepositoryCategory(c))
}

func (p *Postgres) creatCategory(ctx context.Context, c Category) error {
	query := `
	 INSERT INTO categorys VALUES($1, $2)`

	_, err := p.db.ExecContext(ctx, query, c.ID, c.Name)

	return err
}

func (p *Postgres) RegisterAuthor(ctx context.Context, a author.Author) error {
	return p.registerAuthor(ctx, toRepositoryAuthor(a))
}

func (p *Postgres) registerAuthor(ctx context.Context, a Author) error {
	query := `
	INSERT INTO authors VALUES($1, $2, $3, $4, $5)`

	_, err := p.db.ExecContext(ctx, query, a.ID, a.FirstName, a.LastName, a.NickName, a.Email)
	log.Println("err132: ", err)
	return err
}

func (p *Postgres) RegisterBook(ctx context.Context, b book.Book) error {
	return p.registerBook(ctx, toRepositoryBook(b))
}

func (p *Postgres) registerBook(ctx context.Context, b Book) error {
	query := `
	INSERT INTO books VALUES($1, $2, $3, $4, $5)`

	_, err := p.db.ExecContext(ctx, query, b.ID, b.Name, b.AuthorID, b.CategoryID, b.Publisher)

	return err
}

func (p *Postgres) GetBook(ctx context.Context, id uuid.UUID) (book.Book, error) {
	b, err := p.getBook(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return book.Book{}, errs.ErrNotFound
		}
		return book.Book{}, err
	}
	return book.UnmarshalBook(book.UnmarshalBookArgs(b))
}

func (p *Postgres) getBook(ctx context.Context, id uuid.UUID) (Book, error) {
	query := `
	SELECT *FROM books WHERE id=$1`
	var b Book
	if err := p.db.GetContext(ctx, &b, query, id); err != nil {
		return Book{}, err
	}
	return b, nil
}

func (p *Postgres) UpdateBook(ctx context.Context, b book.Book) error {
	return p.updateBook(ctx, toRepositoryBook(b))
}

func (p *Postgres) updateBook(ctx context.Context, b Book) error {
	query := `
	UPDATE books
	       SET name=$1, author_id=$2, category_id=$3, publisher=$4
		   WHERE id=$5
		   `
	_, err := p.db.ExecContext(ctx, query, b.Name, b.AuthorID, b.CategoryID, b.Publisher)

	return err

}

func (p *Postgres) DeleteBook(ctx context.Context, id uuid.UUID) error {
	return p.deleteBook(ctx, id)
}

func (p *Postgres) deleteBook(ctx context.Context, id uuid.UUID) error {
	query := `
	DELETE FROM books WHERE id=$1
	`
	_, err := p.db.ExecContext(ctx, query, id)

	return err
}
