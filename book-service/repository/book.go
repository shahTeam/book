package repository

import "github.com/google/uuid"

//Book represents book.Book struct for repository usage
type Book struct {
	ID         uuid.UUID `db:"id"`
	Name       string    `db:"name"`
	AuthorID   uuid.UUID `db:"author_id"`
	CategoryID uuid.UUID `db:"category_id"`
	Publisher  string    `db:"publisher"`
}