CREATE TABLE categorys (
    id UUID PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

CREATE TABLE authors (
    id UUID PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,  
    nickname VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL
);

CREATE TABLE books (
    id UUID PRIMARY KEY,
    name VARCHAR(60) NOT NULL UNIQUE,
    author_id UUID NOT NULL REFERENCES authors(id),
    category_id UUID NOT NULL REFERENCES categorys(id),
    publisher VARCHAR(70) NOT NULL
);