package repository

import "github.com/google/uuid"

//Category represents category.Category struct for repository usage
type Category struct {
	ID   uuid.UUID `db:"id"`
	Name string    `db:"name"`
}
