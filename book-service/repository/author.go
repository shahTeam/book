package repository

import "github.com/google/uuid"


// Author represents author.Author struct for repository usage
type Author struct {
	ID        uuid.UUID `db:"id"`
	FirstName string    `db:"firstname"`
	LastName  string    `db:"lastname"`
	NickName  string    `db:"nickname"`
	Email     string    `db:"email"`
}
