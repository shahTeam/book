package repository

import (
	"book/domain/author"
	"book/domain/book"
	"book/domain/category"
)



func toRepositoryCategory(c category.Category) Category {
	return Category{
		ID: c.ID(),
		Name: c.Name(),
	}
}

func toRepositoryAuthor(a author.Author) Author {
	return Author{
		ID: a.ID(),
		FirstName: a.FirstName(),
		LastName: a.LastName(),
		NickName: a.NickName(),
		Email: a.Email(),
	}
}

func toRepositoryBook(b book.Book) Book {
	return Book{
		ID: b.ID(),
		Name: b.Name(),
		AuthorID: b.AuthorID(),
		CategoryID: b.CategoryID(),
		Publisher: b.Publisher(),
	}
}

func toDomainCategorys(repoCategorys []Category) ([]category.Category, error) {
	categorys := make([]category.Category, 0, len(repoCategorys))
	for _, repoCategory := range repoCategorys{
        c, err := category.UnmarshalCategory(category.UnmarshalCategoryArgs(repoCategory))
		if err != nil {
			return nil, err 
		}
		categorys = append(categorys, c)
	}
	return categorys, nil
}

func toDomainAuthors(repoAuthors []Author) ([]author.Author, error) {
	authors := make([]author.Author, 0, len(repoAuthors))
	for _, rerepoAuthor := range repoAuthors {
         a, err := author.UnmarshalAuthor(author.UnmarshalAuthorArgs(rerepoAuthor))
		 if err != nil {
			return nil, err
		 }
		 authors = append(authors, a)
	}
	return authors, nil
}

func toDomainBooks(repoBooks []Book) ([]book.Book, error) {
	books := make([]book.Book, 0, len(repoBooks))
	for _, repoBook := range repoBooks{
         b, err := book.UnmarshalBook(book.UnmarshalBookArgs(repoBook))
		 if err != nil {
			return nil, err
		 }
		 books = append(books, b)
	}
	return books, nil
}