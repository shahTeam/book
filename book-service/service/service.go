package service

import (
	"book/domain/author"
	"book/domain/book"
	"book/domain/category"
	"context"
	"github.com/google/uuid"
	
)

type Service struct {
	repo            Repository
	categoryFactory category.Factory
	authorFactory   author.Factory
	bookFactory     book.Factory
}

func New(
	repo Repository, categoryFactory category.Factory, authorFactory author.Factory, bookFactory book.Factory,
) Service {
	return Service{
		repo:            repo,
		categoryFactory: categoryFactory,
		authorFactory:   authorFactory,
		bookFactory:     bookFactory,
	}
}

func (s Service) RegisterCategory(ctx context.Context, c category.Category) (category.Category, error) {
	if err := s.repo.RegisterCategory(ctx, c); err != nil {
		return category.Category{}, err
	}
	return c, nil
}

func (s Service) RegisterAuthor(ctx context.Context, a author.Author) (author.Author, error) {
	if err := s.repo.RegisterAuthor(ctx, a); err != nil {
		return author.Author{}, err
	}
	return a, nil
}

func (s Service) RegisterBook(ctx context.Context, b book.Book) (book.Book, error) {
	if err := s.repo.RegisterBook(ctx, b); err != nil {
		return book.Book{}, err
	}
	return b, nil
}

func (s Service) GetBook(ctx context.Context, id uuid.UUID) (book.Book, error) {
	b, err := s.repo.GetBook(ctx, id)
	if err != nil {
		return book.Book{}, err
	}
	return b, nil
}

func (s Service) UpdateBooks(ctx context.Context, b book.Book) (book.Book, error) {
	if err := s.repo.UpdateBook(ctx, b); err != nil {
		return book.Book{}, err
	}
	return b, nil
}

func (s Service) DeleteBook(ctx context.Context, id uuid.UUID) error {
	return s.repo.DeleteBook(ctx, id)
}
