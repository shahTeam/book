package service

import (
	"context"

	"book/domain/category"

	"book/domain/author"

	"book/domain/book"
	"github.com/google/uuid"
)

type Repository interface {
	CategoryRepository
	AuthorRepository
	BookRepository
}

type CategoryRepository interface {
	RegisterCategory(ctx context.Context, c category.Category) error
}

type AuthorRepository interface {
	RegisterAuthor(ctx context.Context, a author.Author) error
}

type BookRepository interface {
	RegisterBook(ctx context.Context, b book.Book) error
	GetBook(ctx context.Context, id uuid.UUID) (book.Book, error)
	UpdateBook(ctx context.Context, b book.Book) error
	DeleteBook(ctx context.Context, id uuid.UUID) error
}
