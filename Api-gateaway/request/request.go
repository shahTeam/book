package request

type RegisterCategoryRequest struct {
	Name string `json:"name"`
}

type RegisterAuthorRequest struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	NickName  string `json:"nick_name"`
	Email     string `json:"email"`
}

type RegisterBookRequest struct {
	Name       string `json:"name"`
	AuthorId   string `json:"author_id"`
	CategoryId string `json:"category_id"`
	Publisher  string `json:"publisher"`
}

type RegisterBookUpdate struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	AuthorId   string `json:"author_id"`
	CategoryId string `json:"category_id"`
	Publisher  string `json:"publisher"`
}
