package response

type Category struct {
	ID string `json:"id"`
	Name string `json:"name"`
}

type Author struct {
	ID string `json:"id"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	NickName string `json:"nick_name"`
	Email string `json:"email"`
}

type Book struct {
	ID string `json:"id"`
	Name string `json:"name"`
	AuthorId string `json:"author_id"`
	CategoryId string `json:"category_id"`
	Publisher string `json:"publisher"`
}

