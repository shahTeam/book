package service

import (
	"context"
	"google.golang.org/protobuf/types/known/emptypb"

	"api/request"
	"api/response"
)

func New(bookService BookServiceClient) Service {
	return Service{
		Book: bookService,
	}
}

type Service struct {
	Book BookServiceClient
}

type BookServiceClient interface {
	RegisterCategory(context.Context, request.RegisterCategoryRequest) (response.Category, error)
	RegisterAuthor(context.Context, request.RegisterAuthorRequest) (response.Author, error)
	RegisterBook(context.Context, request.RegisterBookRequest) (response.Book, error)
	GetBookID(context.Context, string) (response.Book, error)
	UpdateBook(context.Context, request.RegisterBookUpdate) (response.Book, error)
	DeleteBook(context.Context, string) (*emptypb.Empty, error)
}
