package main

import (
	"api/adapter"
	"api/clients"
	"api/handler"
	"api/service"
	"log"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"


	"context"
	"time"

	"github.com/gin-gonic/gin"
)

const BookServiceUrl = "localhost: 8000"


// @title           Postgres Crud API
// @version         1.0
// @description     This is a sample server celler server.

// NewRoutor
// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io
func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	bookServiceClient, err := clients.NewBookServiceClient(ctx, BookServiceUrl)
	if err != nil {
		log.Panicln("failed to create new book service client:", err)
	}
	bookService := adapter.NewBookService(bookServiceClient)
	service := service.New(bookService)
	h := handler.New(service)

	r := gin.Default()

	r.POST("/category", h.CreatCategory)
	r.POST("/author", h.CreatAuthor)
	r.POST("/book", h.CreatBook)
	r.GET("/book", h.GetBook)
	r.PUT("/book", h.UpdateBook)
	r.DELETE("/book", h.DeleteBook)
	
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":8080")
}
