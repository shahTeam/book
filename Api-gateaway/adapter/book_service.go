package adapter

import (
	"api/bookApp"
	"api/request"
	"api/response"
	"context"
	"log"

	"google.golang.org/protobuf/types/known/emptypb"
)

type BookService struct {
	client bookApp.BookServiceClient
}

func NewBookService(client bookApp.BookServiceClient) BookService {
	return BookService{
		client: client,
	}
}

func (b BookService) RegisterCategory(ctx context.Context, req request.RegisterCategoryRequest) (response.Category, error) {
	grpcRequest := &bookApp.CategoryRequest{
		Name: req.Name,
	}
	res, err := b.client.RegisterCategory(ctx, grpcRequest)
	if err != nil {
		return response.Category{}, err
	}
	return response.Category{
		ID:   res.Id,
		Name: res.Name,
	}, nil
}

func (b BookService) RegisterAuthor(ctx context.Context, req request.RegisterAuthorRequest) (response.Author, error) {
	grpcRequest := &bookApp.AuthorRequest{
		FirstName: req.FirstName,
		LastName:  req.LastName,
		NickName:  req.NickName,
		Email:     req.Email,
	}
	res, err := b.client.RegisterAuthor(ctx, grpcRequest)
	if err != nil {
		return response.Author{}, err
	}

	return response.Author{
		ID:        res.Id,
		FirstName: res.FirstName,
		LastName:  res.LastName,
		NickName:  res.NickName,
		Email:     res.Email,
	}, nil
}

func (b BookService) RegisterBook(ctx context.Context, req request.RegisterBookRequest) (response.Book, error) {
	grpcRequest := &bookApp.BookRequest{
		Name:       req.Name,
		AuthorId:   req.AuthorId,
		CategoryId: req.CategoryId,
		Publisher:  req.Publisher,
	}

	res, err := b.client.RegisterBook(ctx, grpcRequest)
	if err != nil {
		return response.Book{}, err
	}
	return response.Book{
		ID:         res.Id,
		Name:       res.Name,
		AuthorId:   res.AuthorId,
		CategoryId: res.CategoryId,
		Publisher:  res.Publisher,
	}, nil
}

func (b BookService) GetBookID(ctx context.Context, id string) (response.Book, error) {
	res, err := b.client.GetBook(ctx, &bookApp.BookIdRequest{Id: id})
	if err != nil {
		return response.Book{}, err
	}

	return response.Book{
		ID:         res.Id,
		Name:       res.Name,
		AuthorId:   res.AuthorId,
		CategoryId: res.CategoryId,
		Publisher:  res.Publisher,
	}, nil
}

func (b BookService) UpdateBook(ctx context.Context, req request.RegisterBookUpdate) (response.Book, error) {
	grpcRequest := &bookApp.UpdateBookRequest{
		Id:         req.ID,
		Name:       req.Name,
		AuthorId:   req.AuthorId,
		CategoryId: req.CategoryId,
		Publisher:  req.Publisher,
	}
	log.Print(grpcRequest, "grpc")

	res, err := b.client.UpdateBook(ctx, grpcRequest)
	if err != nil {
		return response.Book{}, err
	}

	return response.Book{
		ID:         res.Id,
		Name:       res.Name,
		AuthorId:   res.AuthorId,
		CategoryId: res.CategoryId,
		Publisher:  res.Publisher,
	}, nil
}

func (b BookService) DeleteBook(ctx context.Context, id string) (*emptypb.Empty, error) {
	_, err := b.client.DeleteBook(ctx, &bookApp.BookIdRequest{Id: id})

	if err != nil {
		return nil, err
	}
	return &emptypb.Empty{}, nil
}
