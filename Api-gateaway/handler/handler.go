package handler

import (
	"api/request"
	"api/service"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	 _ "api/docs"
)

func New(svc service.Service) Handler {
	return Handler{
		service: svc,
	}
}

type Handler struct {
	service service.Service
}

// Create Category
// @Summary      Create category
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body request.RegisterCategoryRequest true "Create"
// @Success      200 {object} response.Category
// @Failure      400
// @Failure      500
// @Router       /category [Post]
func (h Handler) CreatCategory(c *gin.Context) {
	var request request.RegisterCategoryRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	book, err := h.service.Book.RegisterCategory(context.Background(), request)
	if err != nil {
		c.JSON(500, gin.H{
			"status": "NOT ok",
		})
		return
	}
	c.JSON(200, book)
}

// Create Author
// @Summary      Create Author
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body request.RegisterAuthorRequest true "Create"
// @Success      200 {object} response.Author
// @Failure      400
// @Failure      500
// @Router       /author [Post]
func (h Handler) CreatAuthor(c *gin.Context) {
	var request request.RegisterAuthorRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	author, err := h.service.Book.RegisterAuthor(context.Background(), request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, author)
}


// Create Book
// @Summary      Create Book
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body request.RegisterBookRequest true "Create"
// @Success      200 {object} response.Book
// @Failure      400
// @Failure      500
// @Router       /book [Post]
func (h Handler) CreatBook(c *gin.Context) {
	var request request.RegisterBookRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	book, err := h.service.Book.RegisterBook(context.Background(), request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, book)
}


// Get Book
// @Summary      Get Book
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        id query string true "Get"
// @Success      200 {object} response.Book
// @Failure      400
// @Failure      500
// @Router       /book [GET]
func (h Handler) GetBook(c *gin.Context) {
	id := c.Query("id")
	book, err := h.service.Book.GetBookID(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, book)
}


// UpdateBook
// @Summary      UpdateBook
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body request.RegisterBookUpdate true "Create"
// @Success      200 {object} response.Book
// @Failure      400
// @Failure      500
// @Router       /book [Put]
func (h Handler) UpdateBook(c *gin.Context) {
	var request request.RegisterBookUpdate
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	book, err := h.service.Book.UpdateBook(context.Background(), request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, book)
}


// DeleteBook
// @Summary      DeleteBook
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        id query string true "Create"
// @Success      200
// @Failure      400
// @Failure      500
// @Router       /book [DELETE]
func (h Handler) DeleteBook(c *gin.Context) {
	id := c.Query("id")
	_, err := h.service.Book.DeleteBook(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Status": "ok",
	})
}
