package clients

import (
	"context"
	"log"

	"api/bookApp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func NewBookServiceClient(ctx context.Context, url string) (bookApp.BookServiceClient, error) {
	conn, err := grpc.DialContext(
		ctx, url,
		grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		log.Println("1: ", err)
		return nil, err
	}
	return bookApp.NewBookServiceClient(conn), nil
}
